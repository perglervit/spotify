var currentPlaylist = [];
var audioElement;
var mouseDown =false;
var currentIndex = 0;
var repeat = false;
var volumeProgress = 0.2;
var shuffleVar = false;
var tempPlaylist = [];
var userLoggedIn;
function openPage(url) {
    if(url.indexOf("?") == -1){
        url = url + "?";
    }
    var encodeUrl = encodeURI(url + "&userLoggedIn=" + userLoggedIn);
    $("#mainContent").load(encodeUrl);
    history.pushState(null,null, url);
    $("body").scrollTop(0);
}
function formatovani(time){
    var finalTime = Math.round(time);
    var minutes =  Math.floor(time /60); //zaokrouhli dolu
    var seconds = finalTime - minutes * 60;
    if(seconds < 10){
        seconds = "0" + seconds; //var extra = (secongs < 10) ? "0" : "";
    }
    return minutes + ":" + seconds;
}

function updateTimeBar(audio) {
    $(".progressTime.current").text(formatovani(audio.currentTime));
    //$(".progressTime.remaining").text(formatovani(audio.duration - audio.currentTime));

    var progress = audio.currentTime / audio.duration * 100;
    $(".playbackBar .progress").css("width", progress + "%");
    $(".playbackBar").hover(function () {
       $(".playbackBar .progress").css("width", 10 + "%");
    });
}

function updateVolumeBar(audio) {
    volumeProgress = audio.volume * 100;
    $(".volumeBar .progress").css("width", volumeProgress + "%");
}


function Audio() {

    this.currentlyPlaying;
    this.audio = document.createElement('audio');
    this.audio.addEventListener("ended",function () {
        nextSong();
    });
    this.audio.addEventListener("canplay", function () {
        //this to with event was called
        $(".progressTime.remaining").text(formatovani(this.duration));
    });

    this.audio.addEventListener("volumechange", function () {
       updateVolumeBar(this);
    });

    this.audio.addEventListener("timeupdate", function () {
        if(this.duration){
            updateTimeBar(this);
        }
    });

    this.setTime = function (seconds) {
        this.audio.currentTime = seconds;
    }

    this.setTrack = function (track) {
        this.currentlyPlaying = track;
        this.audio.src = track.path;
    }
    this.play = function () {
        this.audio.play();
    }
    this.pause = function () {
        this.audio.pause();
    }
}