<?php
    include ("includes/includedFiles.php");

    if(isset($_GET["id"])){
        $artistId = $_GET["id"];

    }else{
        header("Location: index.php");
    }
    $artist = new Artist($con,$artistId);
?>

<div class="entityInfo">
    <div class="centerSection">
        <div class="artistInfo" style="background-image: url('<?php echo $artist->getSecondaryArt();?>');">
            <div class="profile">
                <img src="<?php echo $artist->getPrimaryArt();?>"
            </div>
            <h1 class="artistName">
                <?php echo $artist->getName(); ?>
            </h1>
            <div class="headerButtons" id="playCurrentAlbum">
                <button class="playCurrentAlbum">Play</button>
            </div>
        </div>
    </div>
</div>

<div class="tracklistContainer">
    <ul class="trackList">
        <?php
        $songIdArray = $artist->getSongsId();

        $i = 1;
        foreach($songIdArray as $songId){
            if($i > 5){
                break;
            }
            $albumSong = new Song($con, $songId);
            $albumArtist = $albumSong->getArtist();
            echo "<li class='tracklistRow'>
                        <div class='trackCount'>
                            <img class='play' src='assets/images/icons/play-white.png' alt='' onclick='setTrack(\"". $albumSong->getId() ."\", tempPlaylist, true)'>
                            <span class='trackNumber'>$i</span>
                        </div>
                        <div class='trackInfo'>
                            <span class='trackName'>" .  $albumSong->getTitle() . "</span>
                            <span class='artistName'>" .  $albumArtist->getName() . "</span>
                        </div>
                        <div class='trackOptions'>
                            <img class='optionsButton'src='assets/images/icons/more.png' alt='more'>
                        </div>
                        <div class='trackDuration'>
                            <span class='duration'>" .  $albumSong->getDuration() . "</span>
                        </div>
                    </li>";
            $i++;
        }
        ?>

        <script>

            document.getElementById('playCurrentAlbum').onclick = function() {
                var tempSongsId = '<?php echo json_encode($songIdArray);?>';
                tempPlaylist = JSON.parse(tempSongsId);
                setTrack(tempPlaylist[0],tempPlaylist,true);
            };

            var tempSongsId = '<?php echo json_encode($songIdArray);?>';
            tempPlaylist = JSON.parse(tempSongsId);
        </script>
    </ul>
</div>
<div class="gridViewContainer">
    <?php
    $albumquery = mysqli_query($con, "SELECT * FROM albums WHERE artist='$artistId'");
    while($row = mysqli_fetch_array($albumquery)){
        echo "
                <div class='gridViewItem'>
                    <span class=\"logo\" role=\"link\" tabindex=\"0\" onclick=\"openPage('album.php?id={$row['id']}')\">
                    <img src='{$row['artworkPath']}'>
                    <div class='gridViewInfo'>
                        {$row['title']}
                    </div>
                    </span>
                </div>
                ";
    }
    ?>
</div>