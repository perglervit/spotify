<?php
    $query = mysqli_query($con, "SELECT id FROM songs ORDER BY RAND() LIMIT 15");
    $resultArray = array();
    while($row =mysqli_fetch_array($query)){
        array_push($resultArray, $row["id"]);
    }

    $jsonArray = json_encode($resultArray);
  ?>
<script>
    $( document ).ready(function () {


        var newPlaylist = <?php echo $jsonArray;?>;
        audioElement = new Audio();
        setTrack(newPlaylist[0], newPlaylist, false);
        updateVolumeBar(audioElement.audio);




            $("#nowPlayingContainer").on("mousedown touchstart mousemove touchmove", function (e) {
                e.preventDefault();
            });

            $(".playbackBar .progressBar").mousedown(function () {
                mouseDown = true;
            });
            $(".playbackBar .progressBar").mouseup(function (e) {
                timeFromOffset(e, this);
                mouseDown = false;
            });
            $(".playbackBar .progressBar").mousemove(function (e) {
                if (mouseDown) {
                    timeFromOffset(e, this);
                }
            });

            $(".volumeBar .progressBar").mousedown(function () {
                mouseDown = true;
            });
            $(".volumeBar .progressBar").mouseup(function (e) {
                mouseDown = false;
                if (audioElement.audio.muted) {
                    audioElement.audio.muted = false;
                    $(".controlButton.volume img").attr("src", "assets/images/icons/volume.png");
                }
                var procento = e.offsetX / $(this).width();
                if (procento >= 0 && procento <= 1) {
                    audioElement.audio.volume = procento;
                }
            });
            $(".volumeBar .progressBar").mousemove(function (e) {
                if (mouseDown) {
                    if (audioElement.audio.muted) {
                        audioElement.audio.muted = false;
                        $(".controlButton.volume img").attr("src", "assets/images/icons/volume.png");
                    }
                    var procento = e.offsetX / $(this).width();
                    if (procento >= 0 && procento <= 1) {
                        audioElement.audio.volume = procento;
                    }
                }
            });

        });
    function timeFromOffset(mouse, progressBar) {
        var procent = mouse.offsetX / $(progressBar).width() * 100;
        var seconds = audioElement.audio.duration * (procent / 100);
        audioElement.setTime(seconds);
    }
function previousSong() {
    if(audioElement.audio.currentTime >= 3 || currentIndex == 0){
        audioElement.setTime(0);
    }else{
        currentIndex--;
        setTrack(currentPlaylist[currentIndex],currentPlaylist,true);
    }
}
    function nextSong() {
        if(repeat){
            audioElement.setTime(0);
            playSong();
            return;
        }
        if(currentIndex == currentPlaylist.length - 1){
            currentIndex = 0;
        }else{
            currentIndex++;
        }
        var trackToPlay = shuffleVar ? shufflePlaylist[currentIndex] : currentPlaylist[currentIndex];
        setTrack(trackToPlay, currentPlaylist, true);
    }

    function repeatSong(){
        repeat = !repeat;
        var imageName =  repeat ? "repeat-active.png" : "repeat.png";
        $(".controlButton.repeat img").attr("src","assets/images/icons/" + imageName);
    }

    function mute(){
        audioElement.audio.muted = !audioElement.audio.muted;
        var imageName =  audioElement.audio.muted ? "volume-mute.png" : "volume.png";
        $(".controlButton.volume img").attr("src","assets/images/icons/" + imageName);

    }

    function shuffle(){
        shuffleVar = !shuffleVar;
        var imageName =  shuffleVar ? "shuffle-active.png" : "shuffle.png";
        $(".controlButton.shuffle img").attr("src","assets/images/icons/" + imageName);
        if(shuffle){
            shuffleArray(shufflePlaylist);
            currentIndex = shufflePlaylist.indexOf(audioElement.currentlyPlaying.id);
        }else{

        }
    }

    function shuffleArray(a) {
            var j, x, i;
            for (i = a.length - 1; i > 0; i--) {
                j = Math.floor(Math.random() * (i + 1));
                x = a[i];
                a[i] = a[j];
                a[j] = x;
            }
    }

    function setTrack(trackId, newPlaylist, play) {
        if(newPlaylist != currentPlaylist){
            currentPlaylist = newPlaylist;
            shufflePlaylist = currentPlaylist.slice();
            shuffleArray(shufflePlaylist);
        }

        if(shuffleVar == true){
            currentIndex =shufflePlaylist.indexOf(trackId);
        }else{
            currentIndex =currentPlaylist.indexOf(trackId);
        }
        pauseSong();
        $.post("includes/handlers/ajax/getSongJson.php", {songId: trackId}, function (data) {
            var track = JSON.parse(data);
            $(".trackName span").text(track.title);
            $(".trackName span").attr("onclick","openPage('album.php?id=" + track.album + "')");
            $.post("includes/handlers/ajax/getArtistJson.php", {artistId: track.artist}, function (data) {
                var artist = JSON.parse(data);
                $(".artistName span").text(artist.name);
                $(".artistName span").attr("onclick","openPage('artist.php?id=" + artist.id + "')");
            });
            $.post("includes/handlers/ajax/getArtWorkPath.php", {albumId: track.album}, function (data) {
                var album = JSON.parse(data);
                $(".albumLink img").attr("src", album.artworkPath);
                $(".albumLink img").attr("onclick","openPage('album.php?id=" + album.id + "')");

            });
            audioElement.setTrack(track);
            if(play){
                playSong();
            }
        });
    }



    function playSong() {

        if(audioElement.audio.currentTime == 0){
            console.log("Update");
            $.post("includes/handlers/ajax/incrementPlays.php", {songId: audioElement.currentlyPlaying.id});
        }else{
            console.log("no");
        }

        $(".controlButton.play").hide();
        $(".controlButton.pause").show();
        audioElement.play();
    }
    function pauseSong() {
        $(".controlButton.play").show();
        $(".controlButton.pause").hide();
        audioElement.pause();
    }
</script>
<div id="nowPlayingContainer">
    <div id="nowPlayingBar">
        <div id="nowPlayingLeft">
            <div class="content">
                <span class="albumLink">
                    <img role="link" class="albumArtWork" src="">
                <div class="trackInfo">
                    <span class="trackName">
                        <span class="trackNamespan" role=\"link\" tabindex=\"0\"></span>
                    </span>
                    <span class="artistName">
                        <span class="artistNamespan" role=\"link\" tabindex=\"0\"></span>
                    </span>
                </div>
                </span>
            </div>
        </div>
        <div id="nowPlayingCenter">
            <div class="content playerControls">
                <div class="buttons">
                    <button class="controlButton shuffle" title="Shuffle button" onclick="shuffle()">
                        <img src="assets/images/icons/shuffle.png" alt="Shuffle">
                    </button>
                    <button class="controlButton previous" title="Previous button" onclick="previousSong()">
                        <img src="assets/images/icons/previous.png" alt="Previous">
                    </button>
                    <button class="controlButton play" title="Play button" onclick="playSong()">
                        <img src="assets/images/icons/play.png" alt="Play">
                    </button>
                    <button class="controlButton pause" title="Pause button" style="display: none;" onclick="pauseSong()">
                        <img src="assets/images/icons/pause.png" alt="Pause">
                    </button>
                    <button class="controlButton next" title="Next button" onclick="nextSong()">
                        <img src="assets/images/icons/next.png" alt="Next">
                    </button>
                    <button class="controlButton repeat" title="Repeat button" onclick="repeatSong()">
                        <img src="assets/images/icons/repeat.png" alt="Repeat">
                    </button>
                </div>

                <div class="playbackBar">
                    <span class="progressTime current">0.00</span>
                    <div class="progressBar">
                        <div class="progressBarBg">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <span class="progressTime remaining">0.00</span>
                </div>
            </div>
        </div>
        <div id="nowPlayingRight">
            <div class="volumeBar">
                <button class="controlButton volume" title="Volume button" onclick="mute()">
                    <img src="assets/images/icons/volume.png" alt="Volume">
                </button>
                <div class="progressBar">
                    <div class="progressBarBg">
                        <div class="progress"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>