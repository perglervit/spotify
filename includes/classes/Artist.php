<?php

class Artist{

    private $con;
    private $id;

    private $name;
    private $primary;
    private $secondary;

    public function __construct($con, $id){
        $this->con = $con;
        $this->id = $id;

        $artistQuery = mysqli_query($this->con, "SELECT * FROM artists WHERE id='$this->id'");
        $artist = mysqli_fetch_array($artistQuery);

        $this->name=$artist["name"];
        $this->primary=$artist["primaryArt"];
        $this->secondary=$artist["secondaryArt"];
    }

    public function getName(){
        return $this->name;
    }
    public function  getSongsId(){
        $query= mysqli_query($this->con, "SELECT id FROM songs WHERE artist='$this->id' ORDER BY plays DESC");
        $array = array();
        while($row =mysqli_fetch_array($query)){
            array_push($array, $row["id"]);
        }

        return $array;
    }

    public function  getPrimaryArt(){
        return $this->primary;
    }
    public function getSecondaryArt(){
        return $this->secondary;
    }
}