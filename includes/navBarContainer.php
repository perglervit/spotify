<div id="navBarContainer">
    <nav class="navBar">
        <span class="logo" role="link" tabindex="0" onclick="openPage('index.php')">
            <img src="assets/images/icons/logo.png" alt="My logo">
        </span>

        <div class="group">
            <div class="navItem">
                <span role="link" tabindex="0" onclick="openPage('search.php')" class="navItemLink">Search</span>
                <img src="assets/images/icons/search.png" alt="Search icon" class="icon">
            </div>
        </div>
        <div class="group">
            <div class="navItem">
                <span role="link" tabindex="0" onclick="openPage('browse.php')" class="navItemLink">Browse</span>
            </div>
            <div class="navItem">
                <span role="link" tabindex="0" onclick="openPage('yourMusic.php')" class="navItemLink">Your Music</span>
            </div>
            <div class="navItem">
                <span role="link" tabindex="0" onclick="openPage('profile.php')" class="navItemLink">Vít Pergler</span>
            </div>
        </div>
    </nav>
</div>