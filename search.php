<?php
        include("includes/includedFiles.php");
        if(isset($_GET["term"])){
            $term = urldecode($_GET["term"]);
        }else{
            $term = "";
        }
?>

<div class="searchCont">
    <input type="text" class="searchInput" value="<?php echo $term; ?>" placeholder="Start typing..." onfocus="this.value = this.value;">
</div>

<script>
    $(".searchInput").focus();
    $(function () {
       var count;
       $(".searchInput").keyup(function () {
          clearTimeout(count);
          count = setTimeout(function () {
             var val= $(".searchInput").val();
             openPage("search.php?term="+val);
          }, 750);
       });
    });
</script>

<div class="tracklistContainer">
    <h1>Songs</h1>
    <ul class="trackList">
        <?php
            $songFindQuery = mysqli_query($con, "SELECT id FROM songs WHERE title LIKE '$term%' LIMIT 10");
            if(mysqli_num_rows($songFindQuery) > 0){
                $songIdArray = array();
                $i =1;
                while($row = mysqli_fetch_array($songFindQuery)){
                    if($i > 15){
                        break;
                    }
                    array_push($songIdArray, $row['id']);
                    $albumSong = new Song($con, $row['id']);
                    $albumArtist = $albumSong->getArtist();

                    echo "
                        <li class='tracklistRow'>
                            <div class='trackCount'>
                                <img class='play' src='assets/images/icons/play-white.png' onclick='setTrack(". $albumSong->getId()  . ", tempPlaylist, true)'>
                                <span class='trackNumber'>&nbsp;</span>
                            </div>
                                <div class='trackInfo'>
                                    <span class='trackName'>" . $albumSong->getTitle(). "</span>
                                    <span class='artistName'>". $albumArtist->getName() ."</span>
                                </div>
                                <div class='trackOptions'>
						            <img class='optionsButton' src='assets/images/icons/more.png'>
					            </div>
                                <div class='trackDuration'>
                                    <span class='duration'>" . $albumSong->getDuration() . "</span>
                                </div>
                        </li>
                         ";
                }
            }else{
                echo "<span class='DodelatClassuNaNothing'>No songs like " . $term . "</span>";
            }
        ?>
    </ul>
</div>